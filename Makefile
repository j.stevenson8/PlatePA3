CCOMP = gcc
COMPILEFLAGS = -g 

plate: main.o hash.o list.o header.h
	$(CCOMP) $(COMPILEFLAGS) -o plate main.o hash.o list.o 

main.o: main.c hash.c header.h 
	$(CCOMP) $(COMPILEFLAGS) -c main.c hash.c list.c 

hash.o:	hash.c list.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c hash.c list.c 

list.o: list.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c list.c 

make clean:
	rm list.o main.o hash.o plate

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node{
	char* plate;
	char* first;
	char* last;
	struct node* next;
};

typedef struct node* hashEntry;
typedef struct node* ll;

struct node* listInit();

hashEntry* hashInit(int hashsize);

void listAdd(struct node* sent, char* plate, char* firstname, char* lastname);

int listFind(ll sent, char* firstname, char* lastname, char* plate);

int listLen(ll sent);

void listPrint(ll sent);

void listFree(ll sent);

int hash(char* plate, int hashsize);

hashEntry* hashInit(int hashsize);

void hashAdd(hashEntry* hashtable, char* plate, char* first, char* last,int hashsize);

int hashFind(hashEntry* hashtable, char* plate, char* first, char* last,int hashsize);

void hashLoad(hashEntry* hashtable,int hashsize);

void hashDump(hashEntry* hashtable, int cellNum);

void hashFree(hashEntry* hashtable,int hashsize);


/* John Stevenson
 * PA3
 * CSE 222
 */

#include "header.h"

/* main handles all user input and top-level operations for the program. 
 * It should initialize and load a hash table and prompt the user for 
 * commands.
 */
int main(int argc,char** argv)
{
	int hashsize=100; //size of the hash
			  //DEFAULT size of 100
	int temp; //used to store the hashsize from the command line before storing it in hashsize
	char input[256]; //stores input from user
	char plate[32],first[32],last[32]; //stores temporary values for plate, first and last name
	hashEntry* table; //pointer to the hash table
	int num; //used for input parsing with numbers
	char extra[100]; //used for input parsing for extra characters

	if((argc!=2) && (argc!=3)) //if there were not 1 or 2 command line arguments
				   //print an error message and exit
	{
		printf("ERROR: improper call of program\n");
		return 1; //return value of 1 indicates a return due to an error in calling plate
	}

	if(argc==3) //if there were 2 command line arguments
		    //a desired hash size was specified
	{
		if(sscanf(argv[2],"%d",&temp)==1) //parse the second command line argument for the hash size
		{
			if(temp>=1) //if it is a valid hash size
				    //set it as hashsize
			{
				hashsize=temp;
			}
			else //if it was not a valid integer
			     //print an error message and exit
			{
				printf("ERROR: hash size must be an integer greater than or equal to 1\n");
				return 1;
			}
		}
		else //if the second command line argument was not a number
	 	     //print an error message and exit
		{
			printf("ERROR: second argument to the function should be an integer\n");
			return 1;
		}
	}
	
	FILE* fp=fopen(argv[1],"r"); //fp is a file pointer to the database given as the first command line argument

	if(fp==0) //if the file cannot be opened or does not exist
		  //print an error message and exit
	{
		printf("ERROR: invalid database\n");
		return 1;
	}

	table=hashInit(hashsize); //initialize the hash table with the specified hash size or default size 100

	while(fgets(input,256,fp)!=NULL) //loop until you reach the end of the file
	{
		sscanf(input,"%s %s %s",plate,first,last); //parses the database for necessary information and splits it into three variables

		hashAdd(table,plate,first,last,hashsize); //adds the new entry into the hash table
	}
	
	printf("Enter a plate or command: "); //prompts the user to enter a command

	while(fgets(input,256,stdin)!=NULL) //loop until the user hits ctrl+d
	{
		input[strlen(input)-1]='\0'; //gets rid of the newline 

		if(strcmp(input,"*LOAD")==0) //prints the size of each hash entry
		{
			hashLoad(table,hashsize);
		}
		else if(strcmp(input,"*DUMP")==0) //prints the contents of the entire database
		{
			for(int i=0;i<hashsize;i++)
			{
				hashDump(table,i);
			}
		}
		else if(sscanf(input,"*DUMP %d",&num)==1) //prints out the contents of a specified hash entry
		{
			if(num<hashsize) //if the number was valid, print out its entry
			{
				hashDump(table,num);
			}
			else //if the user enters an invalid hash entry, print an error message 
			{
				printf("ERROR: must enter an integer less than %d\n",hashsize);
			}
		}
		else //if no command was called, search for a plate
		{
			if(hashFind(table,input,first,last,hashsize)==1) //if the plate was found, print out its owner
			{
				printf("FIRST NAME: %s\n",first);
				printf("LAST NAME: %s\n",last);
			}
			else //if not, print a message 
			{
				printf("PLATE NOT FOUND\n");
			}
		}

		printf("Enter a plate or command: ");
	}

	hashFree(table,hashsize); //frees the contents of the hash table
	fclose(fp); //closes the database
	printf("\n"); //resets command line 
	return 0; //signals a successful termination of the program
}


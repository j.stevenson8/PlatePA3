/* List.c contains all functions in PA3 involving 
 * lists and their operations. 
 */

#include "header.h"

/* listInit creates an empty linked list and returns a pointer 
 * to its sentinel node. The linked list is initialized and 
 * ready to be used once the function has finished.
 */
ll listInit()
{
	ll sent=malloc(sizeof(struct node)); //allocates memory for the sentinel to a linked list
	sent->next=NULL;//initializes the sentinel node of the linked list to be the only node 
	return sent; //returns the pointer to the sentinel node created
}

/* listAdd adds a license plate to a linked list. It must be 
 * given the pointer to the sentinel node of the linked list 
 * the new plate will be added to, the plate number, and the 
 * owner's first and last name. Used exclusisvely in hashAdd.
 */ 
void listAdd(struct node* sent, char* plate, char* firstname, char* lastname)
{
	ll node=malloc(sizeof(struct node)); //allocates memory for the new entry
	
	node->plate=malloc(strlen(plate)+1);//allocates enough memory to store the string then copies it to the plate field in node
	strcpy(node->plate,plate);

	node->first=malloc(strlen(firstname)+1);
	strcpy(node->first,firstname);

	node->last=malloc(strlen(lastname)+1);
	strcpy(node->last,lastname);
	
	node->next=sent->next;//adds the new node to the beginning of the linked list
	sent->next=node;
	
	return;
}

/* listFind searches a linked list for a specific plate entry.
 * It must be given the pointer to the sentinel node of the 
 * linked list to be searched, the plate number, a pointer to 
 * store the first name in, and a pointer to store the last name 
 * in. It returns a 1 if the plate is found and a 0 if it is note.
 */
int listFind(struct node* node, char* firstname, char* lastname, char* plate)
{
	while(node->next!=NULL)//The condition is true as long as node is not the last node in the linked list
	{
		node=node->next;
		if(strcmp(node->plate,plate)==0)//if the plate stored at this node is the plate we are looking for,
						//load the first and last name into the variable firstname and lastname
						//and return a 1
		{
			strcpy(firstname,node->first);
			strcpy(lastname,node->last);
			return 1;
		}
	}
	//If the program gets to this point, the plate is not in the linked list
	return 0;

}

/* listLen counts the number of data entries in a linked list 
 * and returns that number. It must be given the pointer to the
 * sentinel node of the linked list to be counted.
 */
int listLen(struct node* node)
{
	int count=0;

	while(node->next!=NULL)//loops through the linked list until it hits the last node 
	{
		node=node->next;
		count++;
	}
	
	return count;
}

/* listPrint prints out all data entries in a given linked list.
 * It must be given the pointer to the sentinel node of the linked 
 * list to be printed.
 */
void listPrint(struct node* node)
{
	while(node->next!=NULL) //loops through the list and prints out each entry
	{
		node=node->next;
		printf("Plate: <%s> Name: %s, %s\n",node->plate,node->last,node->first);
	}

	return;
}

/* listFree frees all memory allocated to a list during runtime.
 * It must be given the pointer to the sentinel node of the linked
 * list to be freed. This function should only be called when 
 * termination of the program has been requested by the user.
 */
void listFree(struct node* node)
{
	ll sent=node;
	node=node->next; //starts at the first REAL node of the list
	ll temp;

	while(node!=NULL)//loop through the linked list freeing all nodes behind the current one
	{
		temp=node;
		node=node->next;

		free(temp->plate);
		free(temp->first);
		free(temp->last);
		free(temp);
	}
	
	free(sent); //frees the sentinel node
	return;
}

/* Hash.c contains all the functions involving the 
 * operations of the hash table.
 */

#include "header.h"

/* hashInit initializes a hash table and returns the address 
 * to the first entry in it. It must be given a desired 
 * length for the hash table. Default size is 100.
 */
hashEntry* hashInit(int hashsize)
{
	hashEntry* table=malloc((sizeof(hashEntry))*hashsize); //allocates memory for the pointer of a hash table

	for(int i=0;i<hashsize;i++) //allocates memory for each hash entry
	{
		table[i]=listInit(); //creates a sentinel to a linked list for every hash entry
	}

	return table; //return the address to the hash table
}

/* hashAdd adds a plate to the hashtable. It must be given
 * the plate number, the owner's first and last name,
 * the pointer to the sentinel node of the hashtable, and the hash size.
 */
void hashAdd(hashEntry* hashtable, char* plate, char* first, char* last,int hashsize)
{
	int index=hash(plate,hashsize); //hash the plate and store its value in index
	
	listAdd(hashtable[index],plate,first,last); //add the plate to the hash entry at index
	return;	
}

/* hashFind searches a hash table for a plate entry. It 
 * returns a 1 if the plate is found and a 0 if it was 
 * not. It should also load the first and last name into 
 * memory for the main program if the plate was found.
 * It must be given the pointer to the first entry of the 
 * hash table to be searched, the plate numebr, a pointer 
 * to store the first name, a pointer to store the 
 * last name, and the hash size.
 */
int hashFind(hashEntry* hashtable, char* plate, char* first, char* last,int hashsize)
{
	int index=hash(plate,hashsize); //hash plate

	int result=listFind(hashtable[index],first,last,plate); //search for plate, 
								//if found, result==1
								//if not, result==0
	return result;
}

/* hashLoad prints the length of each entry in the hash table.
 * It must be given the pointer to the first entry of the 
 * hash table to be printed and the hash size of the hash table.
 */
void hashLoad(hashEntry* hashtable,int hashsize)
{
	for(int i=0;i<hashsize;i++) //loop through the hash table and print out the length of each entry
	{
		printf(" Entry %d: length=%d\n",i,listLen(hashtable[i])); //listLen returns the length of each hash entry 
	}

	return;	
}

/* hashDump prints the contents of a single entry in a 
 * hash table. It must be given the pointer to the 
 * first entry of the hash table and the entry's index.
 */
void hashDump(hashEntry* hashtable, int cellNum)
{
	printf("Contents of cell %d\n",cellNum);
	listPrint(hashtable[cellNum]); //prints out the contents of each hash entry (linked list)
	printf("------------------------------------------\n");
	return;
}

/* hashFree frees all memory allocated to the hash table 
 * during runtime. This function should only be called 
 * when termination of the program has been requested 
 * by the user.
 */
void hashFree(hashEntry* hashtable,int hashsize)
{
	for(int i=0;i<hashsize;i++) //frees each hash entry 
	{
		listFree(hashtable[i]);
	}
	
	free(hashtable); //frees pointer to hash table
	return;	
}

/* hash is a function that performs the hashing for 
 * indexing entries. It must be given the plate and 
 * size of the hash table. It returns the hashed 
 * value.
 */

int hash(char* plate, int hashsize)
{
	int sum=0; //initializes sum to 0

	for(int i=0;i<(strlen(plate)-1);i++) //loops through the string
	{
		sum+=(i+5)*plate[i]; //this is the hash for PA3
	}
	
	return (sum%hashsize); //returns the hash modulo hash size
}	
